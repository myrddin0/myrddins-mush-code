module bitbucket.org/myrddin0/MyMUSH

go 1.13

require (
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	go.mongodb.org/mongo-driver v1.3.0
	gopkg.in/yaml.v2 v2.2.8
)
