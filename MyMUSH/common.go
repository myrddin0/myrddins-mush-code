// vim:tw=99999:expandtab
package main

import (
	"context"
	"errors"
	"fmt"
	"net"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type GameMetaData struct {
	next_dbref int
}

type COMMAND_GOCOMM int

const (
	cmd_gc_shutdown COMMAND_GOCOMM = iota
)

type goComm struct {
	cmd COMMAND_GOCOMM
}

const (
	objTypePlayer = iota + 1
	objTypeRoom
	objTypeExit
	objTypeThing
)

type Player struct {
	_id      primitive.ObjectID
	Name     string
	Password string
	DBref    int
	Location int
	Desc     string
	Conn     net.Conn
	State    PLAYER_STATE
	Cmds     int
}

type Room struct {
	_id   primitive.ObjectID
	Name  string
	DBref int
	Owner int
	Desc  string
}

type Exit struct {
	_id         primitive.ObjectID
	Name        string
	DBref       int
	Owner       int
	Desc        string
	Location    int
	Destination int
}

type Thing struct {
	_id      primitive.ObjectID
	Name     string
	DBref    int
	Owner    int
	Desc     string
	Location int
}

type AttrSet struct {
	target      string
	targetDBref int
	attrName    string
	attrVal     string
}

// global maps
var players map[int]*Player
var gameData GameMetaData

// For use in things like emoteRoom(), where we pass a list of dbrefs to skip.
// We can use this as that 'notThese' argument if we want to send to everyone in that room.
var allPlayers []int

func initPlayersMap() {
	log.Notice("iniitializing player map")
	players = make(map[int]*Player)
}

func getNextDbref() int {
	dbref := gameData.next_dbref
	gameData.next_dbref++
	return dbref
}

// func emoteRoom(loc int, msg string, skip int) {
func emoteRoom(loc int, msg string, notThese []int) {
	var filter bson.M
	ctx := context.TODO()

	if len(notThese) == 0 {
		// emote will go to everyone in the room
		filter = bson.M{"location": loc}
	} else {
		// emote will go to everyone in the room except 'notThese'
		filter = bson.M{"location": loc, "dbref": bson.M{"$nin": notThese}}
	}

	cursor, err := mdb.Players.Find(ctx, filter)
	if err != nil {
		// TODO: emote to the player at least?
		log.Error(err)
		return
	}

	for cursor.Next(ctx) {
		var tmpPlayer Player
		cursor.Decode(&tmpPlayer)
		if _, ok := players[tmpPlayer.DBref]; ok {
			tp := players[tmpPlayer.DBref]
			if tp.State == ps_LoggedIn && tp.Conn != nil {
				netWrite(players[tmpPlayer.DBref].Conn, "%v\n", msg)
			}
		}
		// log.Printf("cmd_say: #%v:%v\n", tmpPlayer.DBref, tmpPlayer.Name)
	}
}

// identifyLocalObj load_obj?  so we can get the object, put it in our in-memory map?
func identifyLocalObj(player *Player, targetNameOrig string) (mushObject, error) {
	if targetNameOrig == "" {
		return nil, errors.New("empty target")
	}
	targetName := strings.ToLower(targetNameOrig)

	if targetName[0] == '#' {
		dbref, _ := strconv.Atoi(targetName[1:])
		obj, err := mdb.GetObject(dbref)
		if err != nil {
			return nil, errors.New("I don't see that here")
		}
		switch obj.(type) {
		case *Player:
			return obj, nil
			break
		case *Room:
			break
		/*
			case Exit:
				break
			case Thing:
				break
		*/
		default:
			break
		}
	}

	// wasn't a dbref
	if targetName == "me" {
		log.Infof("#%d:%v is IDing 'me'", player.DBref, player.Name)
		// return interface{}(player), 0
		meTarget, err := mdb.GetPlayer(player.DBref)
		if err != nil {
			return nil, fmt.Errorf("can't find me:%v", player.DBref)
		}
		return meTarget, nil
	}

	if targetName == "here" {
		log.Infof("#%d:%v is IDing 'here'", player.DBref, player.Name)
		room, err := mdb.GetRoom(player.Location)
		if err != nil {
			return nil, fmt.Errorf("can't find here:%v", player.Location)
		}
		return room, nil
	}

	log.Infof("#%d:%v is IDing '%v' locally", player.DBref, player.Name, targetName)

	// at this point, look for other things in the room (other players, things, exits)
	// exits have priority
	// TODO: do an exit search

	// for now, just look for players whose names start with <target_name>
	var filter bson.M
	ctx := context.TODO()

	// emote will go to everyone in the room
	regexName := "/^" + targetName + "/i"
	filter = bson.M{"location": player.Location, "name": regexName}

	cursor, err := mdb.Players.Find(ctx, filter)
	if err != nil {
		// TODO: emote to the player at least?
		// log.Error(err)
		// netWrite(player.Conn, ":: No such object '%v'\n", targetNameOrig)
		return nil, fmt.Errorf("no such object: '%v'", targetNameOrig)
	}

	for cursor.Next(ctx) {
		var tmpPlayer Player
		cursor.Decode(&tmpPlayer)
		log.Infof("found player #%d:%v", tmpPlayer.DBref, tmpPlayer.Name)
		return &tmpPlayer, nil
	}

	return nil, nil
}

func doSet(player *Player, opts *player_cmd_options) *Player {
	o := *opts
	if o.target_dbref == -1 {
		if o.target_name == "" {
			log.Noticef("do_set: no target provided. Player (%v: %v), attr (%v: %v)",
				player.Name, player.DBref, o.attr_name, o.attr_val)
			player.Write("I don't see that here.")
			return player
		}
		// o.target_dbref = dbref_locate(player, o.target_name)
	}
	return player
}

func parseSetCommand(actor mushObject, command string) (*AttrSet, error) {
	var attrSet AttrSet
	if command == "" {
		return &attrSet, nil
	}

	// desc me=a wizard guy
	attrTargetVal := strings.SplitN(command, " ", 2)
	if len(attrTargetVal) > 1 {
		targetVal := strings.SplitN(attrTargetVal[1], "=", 2)
		if len(targetVal) > 1 {
			attrSet.target = strings.TrimSpace(targetVal[0])
			attrSet.attrName = strings.TrimSpace(attrTargetVal[0])
			attrSet.attrVal = strings.TrimSpace(targetVal[1])
		} else {
			actor.WriteError("Bad set command:2")
			return nil, nil
		}
	} else {
		actor.WriteError("Bad set command:1")
		return nil, nil
	}

	return &attrSet, nil
}
