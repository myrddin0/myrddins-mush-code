// vim:tw=9999
package main

import (
	"context"
	"fmt"
	"net"
	"runtime"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	// "go.mongodb.org/mongo-driver/bson/primitive"
	// "go.mongodb.org/mongo-driver/mongo"
	// "go.mongodb.org/mongo-driver/mongo/options"
)

var con_cmds map[string]func(net.Conn, con_cmd_options) *Player
var player_cmds map[string]func(*Player, player_cmd_options) *Player

func initCmds() {
	log.Info("initializing connection command table")
	con_cmds = map[string]func(net.Conn, con_cmd_options) *Player{
		"connect": cmd_connect,
		"create":  cmd_create,
		"who":     cmd_who_connect,
	}

	player_cmds = map[string]func(*Player, player_cmd_options) *Player{
		"@emit":        cmd_emote,
		"l":            cmd_look,
		"look":         cmd_look,
		"@newpassword": cmd_newpassword,
		"pose":         cmd_pose,
		"say":          cmd_say,
		"@set":         cmd_set,
		"th":           cmd_think,
		"who":          cmd_who,

		"@adesc": cmd_set,
		"@desc":  cmd_set,
	}
}

type player_cmd_options struct {
	player       Player
	cmd          string
	cmd_args     string
	cmd_prefix   string
	attr_name    string
	attr_val     string
	target_dbref int
	target_name  string
}

type con_cmd_options struct {
	name string
	pw   string
}

func cmd_emote(player *Player, opts player_cmd_options) *Player {
	p := *player

	msg := fmt.Sprintf("%v", opts.cmd_args)
	emoteRoom(p.Location, msg, allPlayers)

	return player
}

func cmd_look(looker *Player, opts player_cmd_options) *Player {
	// p := *player

	if len(opts.cmd_args) == 0 {
		room, _ := mdb.GetRoom(looker.Location)
		room.View(looker)
		return looker
	}

	target, err := identifyLocalObj(looker, opts.cmd_args)
	if err != nil {
		looker.WriteError(err.Error())
		return looker
	}

	switch target.(type) {
	case *Player:
		target.View(looker)
	case *Room:
		// var room *Room
		// roomIF := target.(Room)
		// room = &roomIF
		// room.View(looker)
		target.View(looker)
	default:
		log.Errorf("type not yet supported")
	}

	return looker
}

func cmd_pose(player *Player, opts player_cmd_options) *Player {
	p := *player

	space := " "
	if opts.cmd == "semipose" {
		space = ""
	}
	msg := fmt.Sprintf("%v%v%v", p.Name, space, opts.cmd_args)
	emoteRoom(p.Location, msg, allPlayers)

	return player
}

func cmd_say(player *Player, opts player_cmd_options) *Player {
	p := *player

	netWrite(p.Conn, "You say, \"%v\"\n", opts.cmd_args)

	msg := fmt.Sprintf("%v says, \"%v\"", p.Name, opts.cmd_args)
	emoteRoom(p.Location, msg, []int{p.DBref})

	return player
}

// we may come in here via:
// &foo me=bar 			cmd_args = 'foo me=bar'
func cmd_set_attr(actor *Player, opts player_cmd_options) *Player {
	target, err := identifyLocalObj(actor, opts.target_name)
	if err != nil {
		actor.WriteError(err.Error())
		return actor
	}
	if target == nil {
		actor.WriteError("nil target")
		return actor
	}

	switch target.(type) {
	case *Player:
		targetPlayer := target.(*Player)
		err = mdb.SetPlayerAttr(targetPlayer, opts.attr_name, opts.attr_val)
	case *Room:
		targetRoom := target.(*Room)
		err = mdb.SetRoomAttr(targetRoom, opts.attr_name, opts.attr_val)
	default:
		log.Errorf("type not yet supported")
	}

	if err != nil {
		actor.WriteError(err.Error())
	} else {
		actor.Write("Set.")
		if strings.ToLower(opts.attr_name) == "desc" {
			target.SetDesc(opts.attr_val)
		}
	}

	return actor
}

// we may come in here via:
// &foo me=bar 			cmd_args = 'foo me=bar'
// @desc me=some desc	cmd_args = 'me=some desc'
// cmd_prefix will be '&' if the former
func cmd_set(player *Player, opts player_cmd_options) *Player {
	return player
}

func cmd_think(player *Player, opts player_cmd_options) *Player {
	p := *player
	netWrite(p.Conn, "%v\n", opts.cmd_args)
	return player
}

func cmd_newpassword(player *Player, opts player_cmd_options) *Player {
	p := *player
	err := mdb.SetPW(player, opts.cmd_args)
	if err != nil {
		netWrite(p.Conn, "ERROR: Unable to change password\n")
		player.Password = opts.cmd_args
	} else {
		netWrite(p.Conn, "Password changed to '%v'\n", opts.cmd_args)
	}
	return player
}

func cmd_connect(c net.Conn, opts con_cmd_options) *Player {
	var player Player

	netWrite(c, ":: attempting connection to %v\n", opts.name)

	// collection := mongoclient.Database(config.MongoDB).Collection("players")
	// filter := bson.D{{"name",opts.name}, {"password",opts.pw}}
	// filter := bson.M{"name": opts.name, "password": opts.pw}
	// rpat := fmt.Sprintf("^%v$",opts.name)
	// filter := bson.D{{Key:"name", Value:bson.D{{"$regex", primitive.Regex{Pattern:rpat, Options:"i"}}}}}
	// filter = append(filter, bson.E{"password", opts.pw})

	filter := bson.M{"lcname": strings.ToLower(opts.name)}

	// netWrite(c, "filter: :%v:\n", filter)

	err := mdb.Players.FindOne(context.TODO(), filter).Decode(&player)

	if err != nil {
		netWrite(c, ":: That player/password combination is invalid\n")
		player.State = ps_ConnScreen
		return &player
	}

	if player.Password != opts.pw {
		netWrite(c, ":: bad pw\n")
		player.State = ps_ConnScreen
		return &player
	}

	netWrite(c, ":: Success! (loc: %v, dbref: %v)\n", player.Location, player.DBref)
	player.Conn = c
	player.State = ps_LoggedIn
	players[player.DBref] = &player
	return &player
}

func cmd_create(c net.Conn, opts con_cmd_options) *Player {
	dbref := getNextDbref()
	player := Player{
		Name:     opts.name,
		Password: opts.pw,
		DBref:    dbref,
		Desc:     "",
		Location: 0}

	// TODO: make sure player name doesn't already exist
	// TODO: dbref generation

	log.Infof("cmd_create: creating '%v' with dbref '#%v'", opts.name, dbref)

	mdb.AddPlayer(&player)
	return cmd_connect(c, opts)
}

// Player Name        On For Idle   Room    Cmds   Host
// Myrddin          1d 03:03   0s$  #0         1   178.128.180.193
// Adrick          58d 17:40   1m   #0     16921   35.238.239.137
// Oleo            58d 19:13   5d$  #4        17   jabberwock.mushpark.com
// 3 Players logged in, 4 record, no maximum.
// func cmd_who(c net.Conn, opts player_cmd_options) *Player {
func cmd_who(player *Player, opts player_cmd_options) *Player {
	// func Caller(skip int) (pc uintptr, file string, line int, ok bool)

	if player == nil {
		funcname, filename, line, _ := runtime.Caller(0)
		log.Infof("%v: %v: %v: NULL player", filename, funcname, line)
		return nil
	}
	plural := "s"
	c := (*player).Conn
	i := 0

	netWrite(c, "%-20s%-8sWhere\n", "Player Name", "dbref")
	for dbref, player := range players {
		dbrefStr := fmt.Sprintf("#%d", dbref)
		locStr := fmt.Sprintf("#%d", player.Location)
		netWrite(c, "%-20s%5s%8s\n", player.Name, dbrefStr, locStr)
		i++
	}

	if i == 1 {
		plural = ""
	}

	netWrite(c, "%d player%s logged in.\n", i, plural)

	return player
}

func cmd_who_connect(c net.Conn, opts con_cmd_options) *Player {
	var p Player
	p.Conn = c
	return cmd_who(&p, player_cmd_options{})
}
