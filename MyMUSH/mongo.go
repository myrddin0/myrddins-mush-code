// vim:tw=9999
package main

import (
	"context"
	"errors"
	"os"
	"strconv"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type mushDB struct {
	DB      *mongo.Database
	Client  *mongo.Client
	Players *mongo.Collection
	Things  *mongo.Collection
	Rooms   *mongo.Collection
	Exits   *mongo.Collection
	Meta    *mongo.Collection
}

var mdb *mushDB

func connectMongo(config YamlConfig) *mongo.Client {
	var creds string
	if config.Mongo.User != "" {
		creds = config.Mongo.User + ":" + config.Mongo.PW + "@"
	}

	URI := "mongodb://" + creds + config.Mongo.Host + ":" + strconv.Itoa(config.Mongo.Port)
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(URI))
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	log.Infof("successful mongodb connection: %v", URI)

	return client
}

func initDb() {
	mdb = &mushDB{
		Client: connectMongo(config),
	}
	// some shorthand
	mc := mdb.Client
	dbname := config.Mongo.DB

	result, err := mc.ListDatabaseNames(context.TODO(), bson.D{{"name", dbname}})
	if err != nil {
		log.Fatal(err)
	}

	for _, db := range result {
		// log.Printf("\t%v\n", db)
		if db == dbname {
			log.Infof("%v database exists", dbname)
			mdb.DB = mc.Database(dbname)
			mdb.setCollections()
			return
		}
	}

	log.Noticef("%v database not found.  Generating fresh database.", dbname)
	mdb.DB = mc.Database(dbname)
	mdb.setCollections()

	res, err := mdb.Meta.InsertOne(context.Background(), bson.M{
		"type":       "database",
		"next_dbref": 0,
	})
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	// create room #0
	res, err = mdb.Rooms.InsertOne(context.Background(), bson.M{"dbref": 0,
		"name":  "Limbo",
		"owner": 1,
		"desc":  "A misty realm that seems to stretch to infinity."})
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
	limbo_id := res.InsertedID

	gameData.next_dbref = 1

	// create Wizard
	wiz := Player{
		Name:     "Wizard",
		Password: "potrzebie",
		DBref:    getNextDbref(),
		Desc:     "",
		Location: 0}
	mdb.AddPlayer(&wiz)
	mdb.UpdateDBrefs()

	log.Noticef("new %v database initialized with Limbo (%v) and Wizard (%v)", dbname, limbo_id, wiz._id)
}

func (mu *mushDB) setCollections() {
	mu.Players = mu.DB.Collection("players")
	mu.Things = mu.DB.Collection("things")
	mu.Rooms = mu.DB.Collection("rooms")
	mu.Exits = mu.DB.Collection("exits")
	mu.Meta = mu.DB.Collection("meta")
}

func (mu *mushDB) AddPlayer(player *Player) (*Player, error) {
	p := *player
	var err error

	res, err := mu.Players.InsertOne(context.Background(), bson.M{
		"dbref":    p.DBref,
		"name":     p.Name,
		"lcname":   strings.ToLower(p.Name),
		"password": p.Password,
		"desc":     "",
		"location": 0})

	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	player._id = res.InsertedID.(primitive.ObjectID)
	mu.UpdateDBrefs()
	return player, nil
}

func (mu *mushDB) SetPW(player *Player, newpw string) error {
	p := *player
	opts := options.Update().SetUpsert(false)
	// filter := bson.D{{"_id", p._id}}
	filter := bson.D{{"dbref", p.DBref}}
	update := bson.D{{"$set", bson.D{{"password", newpw}}}}

	log.Infof("attempting to change password for '%v (#%v)' (%v)", p.Name, p.DBref, p._id)
	result, err := mu.Players.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount == 1 {
		log.Infof("player '%v (#%v)' just changed password", p.Name, p.DBref)
	} else {
		log.Errorf("WARNING: Unable to change password for player '%v (#%v)'", p.Name, p.DBref)
		return errors.New("can't update document")
	}

	return nil
}

func list_dbs(mc *mongo.Client) {
	log.Info("listing mongo databases:")
	result, err := mc.ListDatabaseNames(context.TODO(), bson.D{})
	// result, err := mc.ListDatabaseNames(context.TODO(), bson.D{{"name", "eternal_mists"}})
	if err != nil {
		log.Fatal(err)
	}

	for _, db := range result {
		log.Infof("\t%v", db)
	}
}

func (mu *mushDB) LoadMetaData() error {
	filter := bson.M{"type": "database"}
	err := mu.Meta.FindOne(context.TODO(), filter).Decode(&gameData)
	return err
}

//	filter := bson.D{{"dbref", p.DBref}}
//	update := bson.D{{"$set", bson.D{{"password", newpw}}}}

//	log.Printf("attempting to change password for '%v (#%v)' (%v)\n", p.Name, p.DBref, p._id)
//	result, err := mongoPlayers.UpdateOne(context.TODO(), filter, update, opts)

func (mu *mushDB) UpdateDBrefs() error {
	opts := options.Update().SetUpsert(true)
	filter := bson.D{{"type", "database"}}
	update := bson.D{{"$set", bson.D{{"next_dbref", gameData.next_dbref}}}}

	result, err := mu.Meta.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount != 1 {
		// this can happen if the next_dbref in the db already agrees with our in memory gameData.next_dbref
		log.Notice("INFO: no Upsert.  Probably due to next_dbref in mongo being the same as gameData.next_dbref")
		return errors.New("can't update document")
	}

	return nil
}

func (mu *mushDB) GetRoom(dbref int) (*Room, error) {
	var room Room

	filter := bson.M{"dbref": dbref}
	err := mu.Rooms.FindOne(context.TODO(), filter).Decode(&room)
	if err != nil {
		log.Noticef("invalid room specifier: %v", dbref)
		return nil, err
	}

	return &room, nil
}

func (mu *mushDB) GetPlayer(dbref int) (*Player, error) {
	var player Player

	filter := bson.M{"dbref": dbref}
	err := mu.Players.FindOne(context.TODO(), filter).Decode(&player)
	if err != nil {
		log.Noticef("invalid player specifier: %v", dbref)
		return nil, err
	}

	return &player, nil
}

// GetObject takes an arbitrary dbref, and returns the object to the caller regardless of type
//
// TODO: let's cache found objects, or have a lookup of dbref -> object type so we don't
// have to do sequential table lookups
func (mu *mushDB) GetObject(dbref int) (mushObject, error) {
	obj, err := mu.GetPlayer(dbref)
	if err == nil {
		return obj, nil
	}

	obj, err = mu.GetPlayer(dbref)
	if err == nil {
		return obj, nil
	}

	return nil, errors.New("I don't see that here")
}

func (mu *mushDB) SetPlayerAttr(p *Player, attr string, val string) error {
	opts := options.Update().SetUpsert(false)
	filter := bson.D{{"dbref", p.DBref}}
	update := bson.D{{"$set", bson.D{{attr, val}}}}

	log.Infof("attempting to set '%v: %v' for '%v (#%v)' (%v)", attr, val, p.Name, p.DBref, p._id)
	result, err := mu.Players.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount == 1 {
		log.Infof("p '%v (#%v)' just set '%v: %v'", p.Name, p.DBref, attr, val)
	} else {
		log.Errorf("WARNING: Unable to set '%v: %v' for p '%v (#%v)'", attr, val, p.Name, p.DBref)
		return errors.New("can't update document")
	}

	return nil
}
func (mu *mushDB) SetRoomAttr(r *Room, attr string, val string) error {
	opts := options.Update().SetUpsert(false)
	filter := bson.D{{"dbref", r.DBref}}
	update := bson.D{{"$set", bson.D{{attr, val}}}}

	log.Infof("attempting to set '%v: %v' for '%v (#%v)' (%v)", attr, val, r.Name, r.DBref, r._id)
	result, err := mu.Rooms.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if result.ModifiedCount == 1 {
		log.Infof("r '%v (#%v)' just set '%v: %v'", r.Name, r.DBref, attr, val)
	} else {
		log.Errorf("WARNING: Unable to set '%v: %v' for r '%v (#%v)'", attr, val, r.Name, r.DBref)
		return errors.New("can't update document")
	}

	return nil
}
