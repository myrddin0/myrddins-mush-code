# Myrddin's MUSH Code

Official repository for Myrddin's MUSH code, including the ubiquitous BBS

## Myrddin's BBS
Global, multi-board BBS that's been installed on countless MUSHes since the early '90s.

## Myrddin's mushcron
Soft coded, MUSH version of the Unix 'cron' utility.
Basically, a way to set up scheduled, repeating triggers/events on your game as specific as you need (eg. do a thing every Monday morning at 3am; do a different thing every other day at noon; do something every 2 minutes 24x7, etc)

## Mush Code Formatting
A couple of perl scripts I use to help with my coding, especially 'mcode.pl', a script that can take nicely formatted code (line breaks, indents, comments, etc) and turn it into the single block of text MUSH needs.
I find it invaluable for coding.