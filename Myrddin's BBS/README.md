Myrddin's Global Bulletin Board
===============================

created by Myrddin (@most MUSHes)

Anyone may use this code. I only ask that the CREDITS attribute be kept
on the board, unchanged.

CURRENT VERSION
---------------
###
[5.2.6](https://bitbucket.org/myrddin0/myrddins-mush-code/src/master/Myrddin's%20BBS/v5/) (Released March 30, 2023)

FEATURES
--------

-   Multi-subject/group bulletin board system, coded specifically to be
    used globally.
-   Lockable groups. Locks can be as simple or as complex as needed.
    Groups can have seperate locks for reading and writing, allowing you
    to set up groups that are read-only for the general populace of the
    MUSH (good for IC newspapers, staff-to-player announcements, etc).
    Also included is a wizard command that will automatically lock
    groups based on flags (wizard, admin, staff, etc) or attributes
    (race:were, race:mortal, etc)
-   Unread status. Board keeps track of which messages a player has
    read. Unread messages will be marked as such. The +bbread command
    allows for a player to automatically read all unread messages in any
    group. Likewise, a +bbcatchup command allows users to mark all
    messages in a group as read.
-   Threaded posting support.  Players can reply to specific posts and 
    create inline threads.
-   +bbscan command. This command will give you a brief list of how many
    unread messages are in each group. This command was designed to be
    placed in a player's @aconnect attribute. Combined with the ability
    to read all unread messages in a group, this allows for players to
    quickly and easily stay current with the bulletin board.
-   +bbnew and +bbnext to allow for easy identification and reading of
    unread messages.
-   Split posting. Users can either post a message with a single
    command, or 'split' the posting as they're writing it (similar to
    the way you would compose mail using the Brandy Mailer).
-   Leave/Join groups. Players can remove themselves from groups, making
    the list of groups they see shorter, and allowing them to omit
    groups they have no interest in. Obviously, they can rejoin whenever
    they wish.

### Instructions for Installation:
INTRODUCING THE NEW SUPER-EASY INSTALLER!

Now, you don't even need to pre-create objects then edit the code file
with the correct dbref's (if that didn't make sense, don't worry :) ).

To install the BB, this is all you need to do:

-   Quote the code file to the MUSH via your preferred method. The
    /quote option under tinyfugue works wonderfully.
-   Place the Global BB in the Master Room of the MUSH. The bbpocket
    will have already been placed inside the BB object for convenience.
    Don't worry, objects inside objects in the Master Room are not
    searched for global commands -- having the bbpocket inside the BB
    won't add to your lag one bit. :-)
-   If you are quoting this file to the MUSH with anyone other than \#1
    (god), there are a couple of extra steps you need to take after
    quoting this file:



```
        @attribute/access bb_read=hidden wizard
        @attribute/access bb_omit=hidden wizard
        @attribute/access bb_silent=hidden wizard
```



And that's it! The installer will create the objects for you, set the
code, and do the rest of the work. It even detects differences between
MUSH and MUX and makes the necessary changes (so far this only involves
a difference of a couple flags and the lnum() function)

Peruse the help files, and reach out if you've got any questions or
concerns. If you also wanted to let me know where you're installing it,
that would be fun, too.

### Miscellaneous Notes:

-   The +bblock command only allows for simple locks. For more
    complex/custom locks, all you need to do is modify the CANREAD
    and/or CANWRITE attributes on the groups in question. :-)
-   The 3.0 and above versions were written to be used on TinyMUSH 2.2
    (or later) or TinyMUX. If you have an older version of TinyMUSH, the
    changes are minimal (namely, replacing hasattr() wtih some
    equivalent soft code - there might be some other minor changes)
-   Don't use the bb as god (\#1). The reason for this is that the bb
    stores an attribute on each character that contains the message-id's
    of the messages you've read. TinyMUSH doesn't allow **anything** to
    modify or set attributes on god (\#1), therefore, the bb won't be
    able to keep track of messages you've read, or groups you've omitted
    yourself from. Besides, it's a bad habit to use god (\#1) for
    day-to-day administrative stuff anyhoo. ;-)


