# Myrddin's mushcron, v1.1.0

## What is it?

mushcron is a soft coded mush utility used to trigger events regularly at pre-specified times. It's based on the unix cron utility. You can be as specific or generic as you need. If you want something triggered every 10 minutes, if you want something triggered every night at 3:05am, or if you want something triggered every odd hour on every Thursday in the month of June, it's all possible and quite easy to set up.

## What can I use it for?

There's no way I could come up with a comprehensive list, the uses are vast, and are specific to the needs of your game. Some things I've used mushcron for:

*   Automated nightly shutdown of my mush. (perl scripts backup and restart the game from there)
*   Bulletin Board message timeouts.
*   Experience point accounting on a daily and weekly basis.
*   Keeping track of total time accumulated for players and staff on a weekly and 'since inception' basis.
*   Healing/Damage accounting.

Some other uses I can envision off the top of my head:

*   Weather emits
*   Virtual time emits
*   OOC Room cleanup
*   Nuking of old, unused, incomplete characters (idle-desting)

## Changelog
### 1.1.0 (Released June 23, 2024)

-   v1.0.0 could have, on rare occasion, skipped a trigger if the jobs tasks it was triggering induced enough latency.  This was fixed some time ago, but that change didn't make it to an official release until now, alas.
-   Jobs are now scanned at the beginning of each minute regardless of when cron is started up.  Previously, jobs would be scanned every 60 seconds from the time cron was started up.  If cron was started at 12:22:48, then jobs would be scanned on the 48th second of every minute starting at 12:23:48.  With v1.1.0, if cron is started at 12:22:48, jobs will be scanned every minute starting at 12:23:00.
-   Jobs can now have _'s in their name.  Previously, `CRON_TIME_FOO_BAR` would be read as `CRON_TIME_FOO` and the code would attempt to trigger `CRON_JOB_FOO`.  This has been fixed.

### 1.0.0 (Released circa 1996 ... maybe?)


Copyright ©1996-2024 J. Scott Dorr
