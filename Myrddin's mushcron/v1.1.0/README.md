
# Myrddin's mushcron, v1.1.0


mushcron is a self-triggering bit of soft code that is used to execute other pieces of mush soft code at precisely scheduled intervals. Once installed, it only requires the user to @trigger the 'cron' attribute on the mushcron object the first time. After that, the object runs itself. It has an @startup attribute that insures mushcron will always run, even after the game is shutdown or restarted.

Once a minute, mushcron searches itself for attributes starting with the pattern **CRON\_TIME\_**.  
CRON\_TIME attributes contain a series of fields, each seperated by a **|**. Each field represents a certain category of time (month, date, hour, etc). For each CRON\_TIME attribute, mushcron determines if the current time matches the rules set up in the time fields. If it matches, it then @triggers a likely named CRON\_JOB attribute on itself (ie. if CRON\_TIME\_FOOBAR matches the current time, then mushcron will @trigger CRON\_JOB\_FOOBAR).

The order of the fields:  

```
[month]|[date]|[day of week]|[hour]|[minute]|[args]
```


Blank fields mean that any value will match the field. Matching is case sensitive. Note that the very last field (the field after the final **|** ) is for any arguments you wish to pass to the CRON\_JOB attribute. Arguments will be passed as %0\. Also note that mushcron will look for a CRON\_JOB attribute name that ends with the _third_ 'word' of the CRON\_TIME attribute (words delimited by **_**'s). The significance of that particular behaviour will be obvious in the examples.

One final note. mushcron expects **time()** to return the system time in this format:  

```
Sun Jan 11 20:19:57 1998
```


## Examples

```
CRON_TIME_FOO: ||Sun|03|01|
CRON_JOB_FOO: @pemit #2=Foo.

CRON_TIME_FOO: ||Sun Wed|03|01|Bar.
CRON_JOB_FOO: @pemit #2=%0

CRON_TIME_FOO_1: ||Sun|03|01|
CRON_TIME_FOO_2: ||Wed|20|00|
CRON_JOB_FOO: @pemit #2=Foo.

CRON_TIME_FOO: Jul|15|Sat Sun|00|01
CRON_JOB_FOO: @pemit #2=Hey, your birthday is on a weekend!
```
